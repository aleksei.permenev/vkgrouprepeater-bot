import enum


class AttachmentType(enum.Enum):
    Photo = "photo"
    Link = "link"
